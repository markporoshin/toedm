import numpy as np
from matplotlib import pyplot as plt
from general import sample_covariance_matrix
from general import s_k_l_j
from math import log
from scipy.special import erf
from math import sqrt
import math


def LDA(sample1, sample2):
    p = len(sample1)
    n1 = len(sample1[0])
    n2 = len(sample2[0])
    means1 = np.array([np.mean(row) for row in sample1])
    means2 = np.array([np.mean(row) for row in sample2])
    cov = sample_covariance_matrix(sample1, sample2, means1, means2)
    cov2 = sample_covariance_matrix(sample1, sample2, means1, means2)
    for i in range(24):
        for j in range(24):
            cov2[i][j] /= sqrt(cov2[i][i]) * sqrt(cov[j][j])
    print(cov2)
    a = np.linalg.inv(cov).dot(means1 - means2)
    z1 = 0
    for i in range(n1):
        z1 += a.dot(sample1[:, i])
    z1 /= n1

    z2 = 0
    for i in range(n2):
        z2 += a.dot(sample2[:, i])
    z2 /= n2
    q1, q2 = n1 / (n1 + n2), n2 / (n1 + n2)
    c = (z1 + z2) / 2 + log(q1 / q2)

    plot = lambda sample1, sample2: m_plot(sample1, sample2, a, c)

    print(f"q1={q1} q2={q2}")

    d, d_h = mahalanobis_distance(sample1, sample2, a, cov, z1, z2, p)

    print(a)
    print(c)

    return {
        "draw": plot,
        "koefs": a,
        "intercept": c,
        "classify": lambda array: get_class(array, a, c, q1, q2),
        "contingency table": lambda sample1, sample2: contingency_table(sample1, sample2, a, c, q1, q2),
        "mahalanobis distance": (d, d_h),
        "error_probability": error_probability(q1, q2, d_h),
        "table": lambda sample1, sample2: get_table(sample1, sample2, a, c, q1, q2)
    }


def m_plot(sample1, sample2, a, c, q1=1, q2=1):
    n1 = len(sample1[0])
    n2 = len(sample2[0])
    f = lambda x, y: ((- a[0] * x - a[1] * y) + np.full(x.shape, c)) / a[2]
    x_max = max(max(sample1[0]), max(sample2[0])) + 1
    x_min = min(min(sample1[0]), min(sample2[0])) - 1
    y_max = max(max(sample1[1]), max(sample2[1])) + 1
    y_min = min(min(sample1[1]), min(sample2[1])) - 1

    x = np.linspace(x_min, x_max, 4)
    y = np.linspace(y_min, y_max, 4)
    X, Y = np.meshgrid(x, y)
    Z = f(X, Y)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in range(n1):
        x = sample1[:, i]
        if get_class(x, a, c, q1, q2) == 1:
            ax.scatter(x[0], x[1], x[2], c='r', marker='^')
        else:
            ax.scatter(x[0], x[1], x[2], c='r', marker='x')
    for i in range(n2):
        x = sample2[:, i]
        if get_class(x, a, c, q1, q2) == 0:
            ax.scatter(x[0], x[1], x[2], c='b', marker='^')
        else:
            ax.scatter(x[0], x[1], x[2], c='b', marker='x')
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')
    ax.plot_wireframe(X, Y, Z, color='black')
    plt.show()


def get_class(array, a, c, q1=1, q2=1):
    if array.dot(a) < c + log(q2 / q1):
        return 0
    else:
        return 1


def contingency_table(sample1, sample2, a, c, q1, q2):
    table = np.full((2, 2), 0)
    n1 = len(sample1[0])
    n2 = len(sample2[0])
    for i in range(n1):
        x = sample1[:, i]
        if get_class(x, a, c, q1, q2) == 1:
            table[0, 0] += 1
        else:
            table[1, 0] += 1
    for i in range(n2):
        x = sample2[:, i]
        if get_class(x, a, c, q1, q2) == 0:
            table[1, 1] += 1
        else:
            table[0, 1] += 1


def mahalanobis_distance(sample1, sample2, a, cov, z1, z2, p):
    n1 = len(sample1[0])
    n2 = len(sample2[0])

    s = (a.dot(cov)).dot(a)
    d = (z1 - z2) ** 2 / (s * s)
    d_h = math.fabs((n1 + n2 - p - 3) / (n1 + n2 - 2) * d - p * (1 / n1 + 1 / n2))
    return sqrt(d), sqrt(d_h)


def f(x):
    return 1 / 2 * (1 + erf(x / sqrt(2)))


def error_probability(q1, q2, d_h):
    k = log(q2 / q1)
    # k = 0
    p_2_1 = f((k - 1 / 2 * d_h ** 2) / d_h)
    p_1_2 = f((-k - 1 / 2 * d_h ** 2) / d_h)
    return p_2_1, p_1_2


def get_table(sample1, sample2, a, c, q1=1, q2=1):
    table = np.zeros((2, 2))
    n1 = len(sample1[0])
    n2 = len(sample2[0])
    for i in range(n1):
        x = sample1[:, i]
        clazz = get_class(x, a, c, q1, q2)
        if clazz == 1:
            table[0, 0] += 1
        else:
            table[1, 0] += 1
    for i in range(n2):
        x = sample2[:, i]
        clazz = get_class(x, a, c, q1, q2)
        if clazz == 1:
            table[0, 1] += 1
        else:
            table[1, 1] += 1
    return table
