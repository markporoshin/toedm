from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
import os

root_path = os.path.abspath("../")
data_path = os.path.join(root_path, 'data/german.data-numeric')
if not os.path.exists(data_path):
    print(f"cannot find data file: {data_path}")
    exit(1)

matrix = np.loadtxt(data_path)


def p_dimensional(matrix, cols, p, size):
    samples = np.zeros((size * 2, p))
    markers = np.zeros((size * 2))
    marker1_i = 0
    marker2_i = 0
    index = 0
    for i in range(1000):
        if marker1_i == size and marker2_i == size:
            break

        if matrix[i][24] == 1:
            if marker1_i == size:
                continue
            marker1_i += 1
        else:
            if marker2_i == size:
                continue
            marker2_i += 1
        for j in range(p):
            samples[index][j] = matrix[i][cols[j]]
        markers[index] = matrix[i][24]
        index += 1
    return samples, markers


X, y = p_dimensional(matrix, [1, 2, 23], 3, 100)

classifier = LinearDiscriminantAnalysis(store_covariance=True)
classifier.fit(X, y)
print(classifier.coef_)
print(classifier.covariance_)
print(classifier.intercept_)
print(classifier.means_)

x_max = max(X, key=lambda p: p[0])[0]+1
x_min = min(X, key=lambda p: p[0])[0]-1
y_max = max(X, key=lambda p: p[1])[1]+1
y_min = min(X, key=lambda p: p[1])[1]-1
z_max = max(X, key=lambda p: p[2])[2]+1
z_min = min(X, key=lambda p: p[2])[2]-1

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.scatter(-classifier.intercept_ / classifier.coef_[0][0], 0, 0, marker='*', color='green')
for i, x in enumerate(X):
    if y[i] == 1:
        if classifier.predict([x])[0] == 1:
            ax.scatter(x[0], x[1], x[2], marker='^', color='red')
        else:
            ax.scatter(x[0], x[1], x[2], marker='x', color='red')
    else:
        if classifier.predict([x])[0] == 2:
            ax.scatter(x[0], x[1], x[2], marker='^', color='blue')
        else:
            ax.scatter(x[0], x[1], x[2], marker='x', color='blue')


def f(x, y):
    return ((- classifier.coef_[0][0] * x - classifier.coef_[0][1] * y) - np.full(x.shape, classifier.intercept_)) / classifier.coef_[0][2]


x = np.linspace(x_min, x_max, 4)
y = np.linspace(y_min, y_max, 4)
X, Y = np.meshgrid(x, y)
Z = f(X, Y)
ax.plot_wireframe(X, Y, Z, color='black')

plt.show()
