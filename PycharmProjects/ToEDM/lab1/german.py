import numpy as np
import os
from lab1.LDA import LDA

root_path = os.path.abspath("../ToEDM/")
data_path = os.path.join(root_path, 'data/german.data-numeric')
if not os.path.exists(data_path):
    print(f"cannot find data file: {data_path}")
    exit(1)

matrix = np.loadtxt(data_path)


def p_dimensional(matrix, cols, n, marker, p, start_from=0):
    samples = np.zeros((p, n))
    col1_i = 0
    offset = 0
    for i in range(1000):
        if col1_i >= n:
            break
        if col1_i + offset < start_from:
            offset += 1
            continue
        if matrix[i][24] == marker and col1_i < n:
            for j in range(p):
                samples[j][col1_i] = matrix[i][cols[j]]
            col1_i += 1
    return samples


p = 25
cols = [i for i in range(25)]
col1 = 1
col2 = 2
col3 = 23

n1 = 100
sample1 = p_dimensional(matrix, cols, n1, marker=1, p=p)
print(np.cov(sample1))
n2 = 100
sample2 = p_dimensional(matrix, cols, n2, marker=2, p=p)
print(np.cov(sample2))
cov = np.cov(sample1, sample2)
print(cov)
result = LDA(sample1, sample2)

print(result['error_probability'])
print(result['mahalanobis distance'])
print(result['table'](sample1, sample2))
result['draw'](sample1, sample2)

n3 = 200
sample3 = p_dimensional(matrix, [col1, col2, col3], n3, marker=1, p=p, start_from=20)
n4 = 200
sample4 = p_dimensional(matrix, [col1, col2, col3], n4, marker=2, p=p, start_from=20)
print(result['table'](sample3, sample4))
result['draw'](sample3, sample4)

