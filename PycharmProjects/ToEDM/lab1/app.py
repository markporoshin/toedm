import numpy as np
import matplotlib.pyplot as plt
from lab1.LDA import LDA

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')

mean1 = (5, 5, 5)
cov = [[5, 2, 1],
       [2, 3, 1],
       [-3, 2, 5]]
y = np.random.multivariate_normal(mean1, cov, 100).transpose()
# ax.scatter(y[0], y[1], y[2], c='r', marker='^')


mean2 = (-5, 5, 5)
x = np.random.multivariate_normal(mean2, cov, 100).transpose()
# ax.scatter(x[0], x[1], x[2], c='b', marker='^')

lda = LDA(x, y)
print(f"error probability: {lda['error_probability']}")
print(f"distance: {lda['mahalanobis distance']}")
# lda['draw'](x, y)

y = np.random.multivariate_normal(mean1, cov, 500).transpose()
x = np.random.multivariate_normal(mean2, cov, 500).transpose()
print(f"table {lda['table'](x, y)}")

# plt.show()
