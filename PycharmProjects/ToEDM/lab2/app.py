import numpy as np
import os
from lab2.PCA import PCA
from matplotlib import pyplot as plt
from math import sqrt, fabs

# mean1 = (0, 0, 0)
# cov = [[7, 5, 0],
#         [5,  7,  0],
#         [0,  0,  1]]
mean1 = (0, 0)
cov = [[7, 5],
        [7,  10]]
sample = np.random.multivariate_normal(mean1, cov, 100).transpose()
print(np.cov(sample))
pca = PCA(sample)
z = pca.get_z()
print(np.cov(z))

fig = plt.figure(1)
# ax = fig.add_subplot(111, projection='3d')
plt.scatter(sample[0], sample[1])
k1 = sqrt(fabs(pca.eigenvalue[0]))
k2 = sqrt(fabs(pca.eigenvalue[1]))
t = np.linspace(0, 1, 100)
x_ev = t * pca.eigenvectors[0][0] * k1
y_ev = t * pca.eigenvectors[1][0] * k1
plt.plot(x_ev, y_ev, linewidth=4, c='r')
x_ev = t * pca.eigenvectors[0][1] * k2
y_ev = t * pca.eigenvectors[1][1] * k2
plt.plot(x_ev, y_ev, linewidth=4, c='r')
plt.show()
exit(0)
# ax.scatter(sample[0], sample[1], sample[2], c='b', marker='.')
# t = np.linspace(0, 1, 100)
# k1 = sqrt(fabs(pca.eigenvalue[0]))
# k2 = sqrt(fabs(pca.eigenvalue[1]))
# k3 = sqrt(fabs(pca.eigenvalue[2]))
# x_ev = t * pca.eigenvectors[0][0] * k1
# y_ev = t * pca.eigenvectors[1][0] * k1
# z_ev = t * pca.eigenvectors[2][0] * k1
# ax.plot(x_ev, y_ev, z_ev, linewidth=4, c='r')
# x_ev = t * pca.eigenvectors[0][1] * k2
# y_ev = t * pca.eigenvectors[1][1] * k2
# z_ev = t * pca.eigenvectors[2][1] * k2
# ax.plot(x_ev, y_ev, z_ev, linewidth=4, c='r')
# x_ev = t * pca.eigenvectors[0][2] * k3
# y_ev = t * pca.eigenvectors[1][2] * k3
# z_ev = t * pca.eigenvectors[2][2] * k3
# ax.plot(x_ev, y_ev, z_ev, linewidth=4,  c='r')
# plt.xlim([-10, 10])
# plt.ylim([-10, 10])
# ax.set_zlim(-10, 5)
#
# # fig = plt.figure(2)
# # ax = fig.add_subplot(111, projection='3d')
# # ax.scatter(z[0], z[1], z[2], c='r', marker='^')
# # plt.xlim([-10, 10])
# # plt.ylim([-10, 10])
# # ax.set_zlim(-10, 5)
#
# plt.show()