import numpy as np
from math import sqrt, fabs


class PCA:
    def __init__(self, x):
        self.x = x
        self.p = len(x)
        self.cov = np.cov(x)
        self.eigenvalue, self.eigenvectors = np.linalg.eig(self.cov)

    def get_z(self):
        z = self.eigenvectors.transpose().dot(self.x)
        return z

    def get_z_p(self):
        n = np.count_nonzero(self.eigenvalue > 1)
        z_p = np.zeros((n, self.x.shape[1]))
        z = self.eigenvectors.transpose().dot(self.x)

        j = 0
        for i in range(self.p):
            # assert fabs(self.cov[i][i] - self.eigenvalue[i]) < 1e-6
            if self.eigenvalue[i] > 1:
                z_p[j, :] = z[i, :]
                j += 1
        cov_z = np.cov(z_p)
        return z_p

    def get_a(self):
        mtr = np.identity(self.p)
        for i in range(self.p):
            mtr[i][i] = sqrt(self.eigenvalue[i])
        return self.eigenvectors.dot(mtr)
