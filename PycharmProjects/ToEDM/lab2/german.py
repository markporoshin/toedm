from lab2.PCA import PCA
from general import matprint, centered, normalized
import matplotlib.pyplot as plt
import numpy as np
import os


root_path = os.path.abspath("../")
data_path = os.path.join(root_path, 'data/german.data-numeric')
if not os.path.exists(data_path):
    print(f"cannot find data file: {data_path}")
    exit(1)

matrix = np.loadtxt(data_path)


def p_dimensional(matrix, cols, n, p):
    samples = np.zeros((p, n))
    clazzes = []
    col1_i = 0
    for i in range(n):
        for j in range(p):
            samples[j][col1_i] = matrix[i][cols[j]]
        col1_i += 1
        clazzes.append(matrix[i][24])
    return samples, clazzes


n = 1000
p = 24
cols = [i for i in range(24)]

sample, clazzes = p_dimensional(matrix, cols, n, p=p)

centred_sample = centered(sample)
standatized_sample = centred_sample # normalized(centred_sample)

pca = PCA(standatized_sample)
z = pca.get_z()

matprint(np.cov(z))
cov_x = np.cov(standatized_sample)
cov_z = np.cov(z)
d_x = []
d_z = []
for i in range(24):
    d_x.append(cov_x[i][i])
    d_z.append(cov_z[i][i])
print("---------")
[print("{:.2f}".format(f), end=' ') for f in sorted(d_x, key=lambda x:-x)]
print()
[print("{:.2f}".format(f), end=' ') for f in sorted(d_z, key=lambda x:-x)]
print()
print("---------")
fig = plt.figure(2)
ax = fig.add_subplot(111, projection='3d')
colors = ['r', 'b']
for i in range(n):
    ax.scatter(z[0][i], z[1][i], z[2][i], c=colors[int(clazzes[i]-1)], marker='^')
plt.show()