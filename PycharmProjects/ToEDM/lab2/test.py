import numpy as np
from matplotlib import pyplot as plt
from lab2.PCA import PCA

plt.grid(True)
plt.gca().set_aspect('equal', adjustable='box')
plt.xlim(-10, 10)
plt.ylim(-10, 10)

x = np.arange(1,11)
y = 2 * x + np.random.randn(10)*2
X = np.vstack((x,y))
for i in range(10):
    X[0, i] -= x.mean()
for i in range(10):
    X[1, i] -= y.mean()
m = (x.mean(), y.mean())
# print(X)


pca = PCA(X)

plt.plot(X[0], X[1], 'r*')
x_1 = np.linspace(0, pca.eigenvectors[0][0])
y_1 = np.linspace(0, pca.eigenvectors[1][0])
plt.plot(x_1, y_1)
x_2 = np.linspace(0, pca.eigenvectors[0][1])
y_2 = np.linspace(0, pca.eigenvectors[1][1])
plt.plot(x_2, y_2)
Z, Z2, a = pca.get_z()
plt.show()

plt.grid(True)
plt.gca().set_aspect('equal', adjustable='box')
plt.xlim(-10, 10)
plt.ylim(-10, 10)
plt.plot(Z[0], Z[1], 'r*')
plt.plot(Z2[0], Z2[1], 'g*')
plt.show()
