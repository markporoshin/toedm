import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from general import centered, normalized, matprint
from lab2.PCA import PCA


iris = datasets.load_iris()
data = np.array(iris.data[:, :]).transpose()
clazz = iris.target


centred_data = centered(data)
standardized_data = normalized(centred_data)

matprint(np.cov(standardized_data))

pca = PCA(standardized_data)
pca_data = pca.get_z()

matprint(np.cov(pca_data))

points = pca_data.transpose()

print("A:")
matprint(pca.get_a())

colors = ['r', 'g', 'b']
markers = ['*', 'o', '^']
for i in range(points.shape[0]):
    x = points[i]
    c = clazz[i]
    plt.plot(x[0], x[1], f'{colors[c]}{markers[c]}')
plt.show()
