import numpy as np
from math import sqrt

def s_k_l_j(m, l, j, n_k, means):
    s = 0
    for i in range(n_k):
        s += (m[l][i] - means[l]) * (m[j][i] - means[j])
    return s / (n_k - 1)


def s_k(samples, p, n_k, means):
    s_k = np.zeros((p, p))
    for y in range(p):
        for x in range(p):
            s_k[x, y] = s_k_l_j(samples, y, x, n_k, means)
    return s_k


def sample_covariance_matrix(s1, s2, means1, means2):
    p = len(s1)
    n_1 = len(s1[0])
    n_2 = len(s2[0])
    s1 = s_k(s1, p, n_1, means1)
    s2 = s_k(s2, p, n_2, means2)
    s = ((n_1 - 1) * s1 + (n_2 - 1) * s2) / (n_1 + n_2 - 2)
    return s


def centered(sample):
    signs = len(sample)
    sample_len = len(sample[0])

    means = [np.mean(sign_sample) for sign_sample in sample]

    centered_sample = list(list())
    for sing in range(signs):
        centered_sample.append(list())
        for num in range(sample_len):
            centered_sample[sing].append(sample[sing][num] - means[sing])
    return np.array(centered_sample)


def normalized(sample):
    signs = len(sample)
    sample_len = len(sample[0])

    vars = [sqrt(np.var(sign_sample)) for sign_sample in sample]

    normalized_sample = list(list())
    for sing in range(signs):
        normalized_sample.append(list())
        for num in range(sample_len):
            normalized_sample[sing].append(sample[sing][num] / vars[sing])
    return np.array(normalized_sample)


def matprint(mat, fmt="g"):
    col_maxes = [max([len(("{:"+fmt+"}").format(x)) for x in col]) for col in mat.T]
    for x in mat:
        for i, y in enumerate(x):
            print(("{:"+str(col_maxes[i])+fmt+"}").format(y), end="  ")
        print("")
