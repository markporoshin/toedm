from sklearn.datasets import make_moons
import numpy as np
from math import exp, cos, log
from lab3.svm import SVM
import matplotlib.pyplot as plt


X, y = make_moons(n_samples=100, noise=0.20)
classes = y * 2 - 1
sample = X

gamma = 1

def kernel(x1, x2):
    return exp( -np.linalg.norm(x1-x2) / (2 * gamma ** 2))


svm = SVM(np.inf)
svm.set_kernel(kernel)
svm.fit(sample, classes)

print(f"w: {svm.w}")
print(f"b: {svm.b}")
h = .1
x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

x_grid = xx.ravel()
y_grid = yy.ravel()
xy = np.c_[x_grid, y_grid]

z_grid = np.zeros(x_grid.shape[0])
for i in range(x_grid.shape[0]):
    z_grid[i] = svm.decision_func(xy[i])


fig = plt.figure(1)
Z = z_grid.reshape(xx.shape)
cm = plt.cm.RdBu
plt.contourf(xx, yy, Z, cmap=cm, alpha=.8)
plt.scatter(sample[:, 0], sample[:, 1], c=classes, marker='.', cmap='coolwarm')
plt.scatter(svm.sv_x[:, 0], svm.sv_x[:, 1], c=svm.sv_classes, marker='x', cmap='coolwarm')
plt.title(f"gamma={gamma}")

plt.show()