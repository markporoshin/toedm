# from sklearn import svm
# X = [[0, 0], [1, 1]]
# y = [0, 1]
# clf = svm.SVC(kernel='linear')
# clf.fit(X, y)
#
# print(clf)

from sklearn import svm
import numpy as np


size1, size2 = 2, 1
sample1 = np.array([[-0.41246186, 0.49700069],
                    [0.18127003, -1.08521142]])
sample2 = np.array([[ 2.62038024],
                    [-3.29532647]])

sample = np.concatenate((sample1, sample2), axis=1).transpose()
y = [0, 0, 1]
clf = svm.SVC(kernel='linear')
clf.fit(sample, y)

print(clf)