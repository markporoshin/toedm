import numpy as np
import os
from lab3.svm import SVM
import matplotlib.pyplot as plt
from math import sin, cos, pi, sqrt, exp, log
from random import randint
from lab2.PCA import PCA
from general import centered, normalized
from sklearn.datasets import make_moons
from sklearn import datasets
# from sklearn.decomposition import PCA


def mix_sample(sample, classes, depth=None):
    if depth is None:
        depth = sample.shape[0]
    for _ in range(depth):
        i = randint(0, sample.shape[0] - 1)
        j = randint(0, sample.shape[0] - 1)
        buf = sample[j, :].copy()
        sample[j, :] = sample[i, :]
        sample[i, :] = buf
        buf = classes[j].copy()
        classes[j] = classes[i]
        classes[i] = buf

def cross_validation(sample, clazzes, parts):
    len = sample.shape[0]
    part_len = len // parts

    yield sample[part_len:, :], clazzes[part_len:],  sample[:part_len, :], clazzes[:part_len]

    for i in range(1, parts - 1):
        train_start = i * part_len
        train_end = (i + 1) * part_len
        yield np.concatenate(
            (sample[:train_start, :], sample[train_end:, :]),
            axis=0
        ), \
        np.concatenate(
            (clazzes[:train_start], clazzes[train_end:]),
            axis=0
        ), \
        sample[train_start:train_end, :], \
        clazzes[train_start:train_end]

    yield sample[:len-part_len, :],  clazzes[:len-part_len], sample[len-part_len:, :], clazzes[len-part_len:]


class Lab3:
    @staticmethod
    def part1():
        """
        research modal, visualized, separable data
        """

        mean1 = (0, 0)
        mean2 = (2, -2)
        cov = [[1, 0.7],
               [0.7, 1]]
        size1, size2 = 100, 100
        sample1 = np.random.multivariate_normal(mean1, cov, size1).transpose()
        sample2 = np.random.multivariate_normal(mean2, cov, size2).transpose()
        sample = np.concatenate((sample1, sample2), axis=1)
        clazzes = np.zeros(size1 + size2)
        for i in range(size1):
            clazzes[i] = 1
        for i in range(size1, size1 + size2):
            clazzes[i] = -1
        svm = SVM(np.inf)
        svm.fit(sample.transpose(), clazzes)

        plt.figure(2)
        x = np.linspace(-2, 5, 10)
        y = (-svm.b - svm.w[0] * x) / svm.w[1]
        y_border_1 = (-svm.b - 1 - svm.w[0] * x) / svm.w[1]
        y_border_2 = (-svm.b + 1 - svm.w[0] * x) / svm.w[1]

        plt.plot(x, y, 'k-')
        plt.plot(x, y_border_1, 'k--')
        plt.plot(x, y_border_2, 'k--')
        plt.scatter(sample.transpose()[:, 0], sample.transpose()[:, 1], c=clazzes, marker='.', cmap='coolwarm',
                    label="эл-ты выборки")
        plt.scatter(svm.sv_x[:, 0], svm.sv_x[:, 1], c=svm.sv_classes, marker='x', cmap='coolwarm',
                    label="опорные векторы")
        plt.legend()
        plt.grid(True)
        plt.show()

        print(svm.w)
        print(2 / np.linalg.norm(svm.w))

    @staticmethod
    def part2():
        mean1 = (0, 0)
        mean2 = (2, -2)
        cov = [[1, 0.3],
               [0.3, 1]]
        size1, size2 = 100, 100
        sample1 = np.random.multivariate_normal(mean1, cov, size1).transpose()
        sample2 = np.random.multivariate_normal(mean2, cov, size2).transpose()
        sample = np.concatenate((sample1, sample2), axis=1)
        clazzes = np.zeros(size1 + size2)
        for i in range(size1):
            clazzes[i] = 1
        for i in range(size1, size1 + size2):
            clazzes[i] = -1

        c_list = [1, 10, 100, 1000]
        svm_colors = ['r', 'g', 'b', 'm']
        fig, axs = plt.subplots(2, 2)
        plt.xlim([-5, 7])
        plt.ylim([-5, 7])
        for i, (c, color) in enumerate(zip(c_list, svm_colors)):
            axs[i // 2, i % 2].scatter(sample.transpose()[:, 0], sample.transpose()[:, 1], c=clazzes, marker='.',
                                       cmap='coolwarm',
                                       label="эл-ты выборки")
            svm = SVM(c)
            svm.fit(sample.transpose(), clazzes)
            x = np.linspace(-2, 5, 10)
            y = (-svm.b - svm.w[0] * x) / svm.w[1]
            y_border_1 = (-svm.b - 1 - svm.w[0] * x) / svm.w[1]
            y_border_2 = (-svm.b + 1 - svm.w[0] * x) / svm.w[1]
            axs[i // 2, i % 2].plot(x, y, color=color, linestyle='-', label=f'svm for c={c}')
            axs[i // 2, i % 2].plot(x, y_border_1, color=color, linestyle='--')
            axs[i // 2, i % 2].plot(x, y_border_2, color=color, linestyle='--')
            axs[i // 2, i % 2].scatter(svm.sv_x[:, 0], svm.sv_x[:, 1], c=svm.sv_classes, marker='x', cmap='coolwarm',
                        label="опорные векторы")
            print(f"c: {c}")
            print(f"w: {svm.w}")
            axs[i // 2, i % 2].set_title(f'w={svm.w}\n M={2 / np.linalg.norm(svm.w)}')
            axs[i // 2, i % 2].legend()
            axs[i // 2, i % 2].grid(True)
        plt.show()

    @staticmethod
    def part3_2():
        mean1 = (0, 0)
        mean2 = (2, -2)
        cov = [[1, 0.3],
               [0.3, 1]]
        size1, size2 = 300, 300
        sample1 = np.random.multivariate_normal(mean1, cov, size1).transpose()
        sample2 = np.random.multivariate_normal(mean2, cov, size2).transpose()
        sample = np.concatenate((sample1, sample2), axis=1).transpose()
        clazzes = np.zeros(size1 + size2)
        for i in range(size1):
            clazzes[i] = 1
        for i in range(size1, size1 + size2):
            clazzes[i] = -1

        c = 1
        for train, train_c, test, test_c in cross_validation(sample, clazzes, 5):
            svm = SVM(c)
            svm.fit(train, train_c)

            pred_clazzes = svm.predict(test)

            errors = np.count_nonzero(pred_clazzes != test_c)
            print(f"error prob: {errors / test_c.shape[0]}")
            print(f"num of sv: {len(svm.sv_indexes)}")

    @staticmethod
    def part3():
        mean1 = (0, 0)
        mean2 = (3, 0)
        cov1 = [[1, 0],
                [0, 2 * pi]]
        cov2 = [[0.3, 0],
                [0, 2 * pi]]
        size1, size2 = 100, 500
        polar_sample1 = np.random.multivariate_normal(mean1, cov1, size1).transpose()
        polar_sample2 = np.random.multivariate_normal(mean2, cov2, size2).transpose()
        polar_sample = np.concatenate((polar_sample1, polar_sample2), axis=1)
        clazzes = np.zeros(size1 + size2)
        for i in range(size1):
            clazzes[i] = 1
        for i in range(size1, size1 + size2):
            clazzes[i] = -1
        sample = np.zeros(polar_sample.transpose().shape)
        for i, x in enumerate(polar_sample.transpose()):
            sample[i, 0] = x[0] * sin(x[1])
            sample[i, 1] = x[0] * cos(x[1])
        kernel_sample = np.zeros((sample.shape[0], 2))
        for i, x in enumerate(sample):
            kernel_sample[i, 0] = np.linalg.norm(x)
            kernel_sample[i, 1] = np.linalg.norm(x)

        svm = SVM(100)

        def kernel(x1, x2):
            return np.linalg.norm(x1) * np.linalg.norm(x2)

        def phi(x):
            return np.array([np.linalg.norm(x)])

        svm.set_kernel(kernel, phi)
        svm.fit(sample, clazzes)

        t = np.linspace(0, 2 * pi, 100)
        x = (-svm.b / svm.w[0]) * np.cos(t)
        y = (-svm.b / svm.w[0]) * np.sin(t)

        x_l_b = ((-svm.b - 1) / svm.w[0]) * np.cos(t)
        y_l_b = ((-svm.b - 1) / svm.w[0]) * np.sin(t)

        x_r_b = ((-svm.b + 1) / svm.w[0]) * np.cos(t)
        y_r_b = ((-svm.b + 1) / svm.w[0]) * np.sin(t)

        plt.plot(x, y, 'k-')
        plt.plot(x_l_b, y_l_b, 'k--')
        plt.plot(x_r_b, y_r_b, 'k--')

        plt.scatter(sample[:, 0], sample[:, 1], c=clazzes, marker='.', cmap='coolwarm',
                    label="эл-ты выборки")
        plt.scatter(svm.sv_x[:, 0], svm.sv_x[:, 1], c=svm.sv_classes, marker='x', cmap='coolwarm',
                   label="опорные векторы")
        plt.legend()
        plt.grid(True)
        plt.show()

        h_sample = np.zeros((sample.shape[0], sample.shape[1] + 1))
        for i, x in enumerate(sample):
            h_sample[i, :sample.shape[1]] = sample[i]
            h_sample[i, sample.shape[1]] = phi(x)
        fig = plt.figure(1)
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(h_sample[:, 0], h_sample[:, 1], h_sample[:, 2], c=clazzes, marker='.', cmap='coolwarm',
                        label="эл-ты выборки")
        def f(shape):
            return np.full(shape, -svm.b / svm.w[0])

        def f_border_1(shape):
            return np.full(shape, -(svm.b + 1) / svm.w[0])

        def f_border_2(shape):
            return np.full(shape, -(svm.b - 1) / svm.w[0])

        x_min, x_max = -4, 4
        y_min, y_max = -4, 4
        x = np.linspace(x_min, x_max, 4)
        y = np.linspace(y_min, y_max, 4)
        X, Y = np.meshgrid(x, y)
        Z = f(X.shape)
        Z_border_1 = f_border_1(X.shape)
        Z_border_2 = f_border_2(X.shape)
        ax.plot_wireframe(X, Y, Z, color='black')
        ax.plot_wireframe(X, Y, Z_border_1, color='black')
        ax.plot_wireframe(X, Y, Z_border_2, color='black')
        plt.show()
        print(svm.w)
        print(svm.b)

    @staticmethod
    def part4_1():
        root_path = os.path.abspath("../")
        data_path = os.path.join(root_path, 'data/german.data-numeric')
        if not os.path.exists(data_path):
            print(f"cannot find data file: {data_path}")
            exit(1)
        matrix = np.loadtxt(data_path)

        sample = matrix[:, :24]
        centred_sample = centered(sample.transpose()).transpose()
        standatized_sample = normalized(centred_sample.transpose()).transpose()
        clazzes = 2 * matrix[:, 24] - 3

        pca = PCA(standatized_sample.transpose())
        pca_sample = pca.get_z_p().transpose()



    @staticmethod
    def part4_3():
        root_path = os.path.abspath("../")
        data_path = os.path.join(root_path, 'data/german.data-numeric')
        if not os.path.exists(data_path):
            print(f"cannot find data file: {data_path}")
            exit(1)
        matrix = np.loadtxt(data_path)

        sample = matrix[:, :24]
        centred_sample = centered(sample)
        standatized_sample = normalized(centred_sample)
        clazzes = 2 * matrix[:, 24] - 3

        pca = PCA(standatized_sample.transpose())
        pca_sample = pca.get_z_p().transpose()
        # pca_sample = sample

        c_list = [1, 10, 100, 1000]
        train_borders = [[1, 200]]
        for c in c_list:
            for bordes in train_borders:
                left = bordes[0]
                right = bordes[1]
                train_sample = np.concatenate((pca_sample[: left, :], pca_sample[right: , :]), axis=0)
                train_classes = np.concatenate((clazzes[: left], clazzes[right:]))
                test_sample = pca_sample[left:right, :]
                test_classes = clazzes[left:right]

                svm = SVM(c)
                svm.fit(train_sample, train_classes)
                predicted_classes = svm.predict(test_sample)
                overlap = predicted_classes == test_classes
                error_probability = 1 - np.count_nonzero(overlap) / overlap.shape[0]
                print("-------------------------------")
                print(f"borders from {left} to {right}")
                print(f"for c={c} error probability is {error_probability}")
                print(f"{svm.w} {svm.b} {svm.sv_indexes}")

    @staticmethod
    def part4_2():
        root_path = os.path.abspath("../")
        data_path = os.path.join(root_path, 'data/german.data-numeric')
        if not os.path.exists(data_path):
            print(f"cannot find data file: {data_path}")
            exit(1)
        matrix = np.loadtxt(data_path)

        sample = matrix[:, :24]
        centred_sample = centered(sample)
        standatized_sample = normalized(centred_sample)
        clazzes = 2 * matrix[:, 24] - 3

        pca_sample = sample

        train_sample = pca_sample[: 800, :]
        train_classes = clazzes[: 800]
        test_sample = pca_sample[800:, :]
        test_classes = clazzes[800:]

        c_list = [1, 10, 100, 1000]
        for c in c_list:
            svm = SVM(c)
            q = 10

            def kernel(x1, x2):
                return exp(- q * np.linalg.norm(x1 - x2))

            svm.set_kernel(kernel, None)
            svm.fit(train_sample, train_classes)
            predicted_classes = svm.predict(test_sample)
            overlap = predicted_classes == test_classes
            error_probability = 1 - np.count_nonzero(overlap) / overlap.shape[0]
            print("-------------------------------")
            print(f"for c={c} error probability is {error_probability}")
            print(f"{svm.w} {svm.b}")

    @staticmethod
    def part5_1():
        X, y = make_moons(n_samples=100, noise=0.10)
        classes = y * 2 - 1
        sample = X

        def phi(x):
            return np.array([exp(x[1] - cos(x[0] * 3) / 2)])

        def kernel(x1, x2):
            return phi(x1) * phi(x2)

        svm = SVM(np.inf)
        svm.set_kernel(kernel, phi)
        svm.fit(sample, classes)

        print(f"w: {svm.w}")
        print(f"b: {svm.b}")

        h_sample = np.zeros((sample.shape[0], sample.shape[1] + 1))
        for i, x in enumerate(sample):
            h_sample[i, :sample.shape[1]] = sample[i]
            h_sample[i, sample.shape[1]] = phi(x)[0]

        x = np.linspace(-2, 2, 100)
        y = np.cos(x * 3) / 2 + log(-svm.b / svm.w[0])
        y_b_l = np.cos(x * 3) / 2 + log(-(svm.b-1) / svm.w[0])
        y_b_r = np.cos(x * 3) / 2 + log(-(svm.b+1) / svm.w[0])
        plt.plot(x, y, 'k-')
        plt.plot(x, y_b_l, 'k--')
        plt.plot(x, y_b_r, 'k--')
        plt.scatter(sample[:, 0], sample[:, 1], c=classes, cmap='coolwarm')
        plt.show()

        fig = plt.figure(1)
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(h_sample[:, 0], h_sample[:, 1], h_sample[:, 2], c=classes, marker='.', cmap='coolwarm')

        def f(shape):
            return np.full(shape, -svm.b / svm.w[0])

        def f_border_1(shape):
            return np.full(shape, -(svm.b + 1) / svm.w[0])

        def f_border_2(shape):
            return np.full(shape, -(svm.b - 1) / svm.w[0])

        x_min, x_max = -1, 1
        y_min, y_max = -1, 1
        x = np.linspace(x_min, x_max, 4)
        y = np.linspace(y_min, y_max, 4)
        X, Y = np.meshgrid(x, y)
        Z = f(X.shape)
        Z_border_1 = f_border_1(X.shape)
        Z_border_2 = f_border_2(X.shape)
        ax.plot_wireframe(X, Y, Z, color='black')
        ax.plot_wireframe(X, Y, Z_border_1, color='black')
        ax.plot_wireframe(X, Y, Z_border_2, color='black')
        plt.show()

    @staticmethod
    def part5_2():
        X, y = make_moons(n_samples=100, noise=0.10)
        classes = y * 2 - 1
        sample = X

        def phi(x):
            return np.array([exp(x[1] - cos(x[0] * 3) / 2)])

        q = 1
        def kernel(x1, x2):
            return exp( - q * np.linalg.norm(x1 - x2))

        svm = SVM(np.inf)
        svm.set_kernel(kernel, None)
        svm.fit(sample, classes)

        print(f"w: {svm.w}")
        print(f"b: {svm.b}")
        fig, axs = plt.subplots(2)
        axs[0].scatter(sample[:, 0], sample[:, 1], c=classes, marker='.', cmap='coolwarm',
                    label="эл-ты выборки")
        axs[0].scatter(svm.sv_x[:, 0], svm.sv_x[:, 1], c=svm.sv_classes, marker='x', cmap='coolwarm',
                    label="опорные векторы")
        axs[0].legend()
        axs[0].grid(True)

        predicted_classes = svm.predict(sample)

        axs[1].scatter(sample[:, 0], sample[:, 1], c=predicted_classes, marker='.', cmap='winter',
                    label="эл-ты выборки c предсказанными классами")
        axs[1].legend()
        axs[1].grid(True)
        plt.show()

    @staticmethod
    def part4_next_gen():
        root_path = os.path.abspath("../")
        data_path = os.path.join(root_path, 'data/german.data-numeric')
        if not os.path.exists(data_path):
            print(f"cannot find data file: {data_path}")
            exit(1)
        matrix = np.loadtxt(data_path)
        sample = matrix[:, :24]
        centred_sample = centered(sample.transpose()).transpose()
        standatized_sample = normalized(centred_sample.transpose()).transpose()
        pca_sample = PCA(standatized_sample.transpose()).get_z_p().transpose()
        classes = 2 * matrix[:, 24] - 3
        mix_sample(pca_sample, classes)

        cv_sample, cv_classes = pca_sample[:800, :], classes[:800]
        cv_test, cv_test_classes = pca_sample[800:, :], classes[800:]

        best_proba = np.inf
        best_svm = None
        for train, train_classes, test, test_classes in cross_validation(cv_sample, cv_classes, 4):
            c_list = [1, 10, 100, np.inf]
            for c in c_list:
                print(f"sizes {train.shape} {test.shape}")
                svm = SVM(c)
                svm.fit(train, train_classes)
                predicted_classes = svm.predict(test)
                errors = np.count_nonzero(predicted_classes != test_classes)
                error_probability = errors / test_classes.shape[0]
                print(f"c={c} P_error={error_probability}")
                if best_svm is None:
                    best_svm = svm
                if best_proba > error_probability:
                    best_proba = error_probability
                    best_svm = svm
        predicted_cv_classes = best_svm.predict(cv_test)
        errors = np.count_nonzero(predicted_cv_classes != cv_test_classes)
        error_probability = errors / cv_test_classes.shape[0]
        print(f"final c={best_svm.C} P_error={error_probability} best_P={best_proba}")

    @staticmethod
    def part4_next_gen_gauss():
        root_path = os.path.abspath("../")
        data_path = os.path.join(root_path, 'data/german.data-numeric')
        if not os.path.exists(data_path):
            print(f"cannot find data file: {data_path}")
            exit(1)
        matrix = np.loadtxt(data_path)
        sample = matrix[:, :24]
        centred_sample = centered(sample.transpose()).transpose()
        standatized_sample = normalized(centred_sample.transpose()).transpose()
        pca_sample = PCA(standatized_sample.transpose()).get_z_p().transpose()
        classes = 2 * matrix[:, 24] - 3
        mix_sample(pca_sample, classes)

        cv_sample, cv_classes = pca_sample[:800, :], classes[:800]
        cv_test, cv_test_classes = pca_sample[800:, :], classes[800:]

        best_proba = np.inf
        best_svm = None
        for train, train_classes, test, test_classes in cross_validation(cv_sample, cv_classes, 4):
            c_list = [1, 10, 100]
            for c in c_list:
                gamma_list = [1, 10, 100]
                for gamma in gamma_list:
                    def kernel(x1, x2):
                        return exp(-np.linalg.norm(x1 - x2) / (2 * gamma ** 2))
                    svm = SVM(c)
                    svm.set_kernel(kernel)
                    svm.fit(train, train_classes)
                    predicted_classes = svm.predict(test)
                    errors = np.count_nonzero(predicted_classes != test_classes)
                    error_probability = errors / test_classes.shape[0]
                    print(f"gamma={gamma} c={c} P_error={error_probability}")
                    if best_svm is None:
                        best_svm = svm
                    if best_proba > error_probability:
                        best_proba = error_probability
                        best_svm = svm
        predicted_cv_classes = best_svm.predict(cv_test)
        errors = np.count_nonzero(predicted_cv_classes != cv_test_classes)
        error_probability = errors / cv_test_classes.shape[0]
        print(f"final c={best_svm.C} P_error={error_probability} best_P={best_proba}")


Lab3.part4_next_gen_gauss()
