import numpy as np
import quadprog
import cvxopt

from typing import Callable
from scipy.optimize import minimize


class SVM:

    def __init__(self, C):
        self.kernel = lambda x_i, x_j: x_i.dot(x_j)
        self.x = None
        self.y = None
        self.solver = None
        self.l = 0
        self.C = C
        self.w = None
        self.b = None
        self.a = None
        self.sv_indexes = None
        self.sv_x = []
        self.sv_classes = []
        self.phi = lambda x : x

    def set_kernel(self, kernel: Callable[[np.array, np.array], float], phi=None):
        self.phi = phi
        self.kernel = kernel

    def fit(self, x, y):
        self.x = x
        self.y = y
        self.n = x.shape[1]
        self.l = x.shape[0]

        H = self._P()
        c = -self._q()
        A, b = self._A_b()

        solver = QuadraticSolverScipyMtr(H, c, A, b, C=self.C)
        result = solver.solve(np.zeros(self.l))
        a = self.a = result['x']
        sv = a > 1e-3
        self.sv_indexes = np.arange(len(a))[sv]
        for index in self.sv_indexes:
            self.sv_x.append(self.x[index])
            self.sv_classes.append(self.y[index])
        self.sv_x = np.array(self.sv_x)
        self.b = 0
        for i in self.sv_indexes:
            for j in range(self.l):
                b += a[j] * y[j] * self.kernel(x[i], x[j])
        b /= len(self.sv_indexes)
        if self.phi is None:
            return
        if len(self.sv_indexes) == 0:
            self.w = a[0] * y[0] * self.phi(x[0]) * 0
        self.w = a[self.sv_indexes[0]] * y[self.sv_indexes[0]] * self.phi(x[self.sv_indexes[0]])
        for j in range(1, len(self.sv_indexes)):
            self.w += a[self.sv_indexes[j]] * y[self.sv_indexes[j]] * self.phi(x[self.sv_indexes[j]])
        # self.b = 0
        # for i in self.sv_indexes:
        #     self.b += 1 / y[i] - self.w.dot(self.phi(x[i]))
        # self.b /= len(self.sv_indexes)

    def decision_func(self, x):
        fun = 0
        for i in range(self.l):
            fun += self.a[i] * self.y[i] * self.kernel(self.x[i], x)
        return fun

    def is_support_vector(self, x, clazz):
        if (self.w.dot(x) + self.b) * clazz - 1 < 1e-3:
            return True
        return False

    def predict_simple(self, x):
        if self.phi is None:
            s = 0
            for i in range(self.l):
                s += self.a[i] * self.y[i] * self.kernel(self.x[i], x)
            return 1 if s > 0 else -1
        return 1 if self.w.dot(x) + self.b > 0 else -1

    def predict(self, sample):
        classes = []
        for x in sample:
            classes.append(self.predict_simple(x))
        return np.array(classes)

    def _P(self):
        x = self.x
        y = self.y
        P = np.zeros((self.l, self.l))
        for i in range(self.l):
            for j in range(self.l):
                if i == j:
                    P[i, j] = float(y[i] * y[j] * self.kernel(x[i], x[j]))
                else:
                    P[i, j] = float(y[i] * y[j] * self.kernel(x[i], x[j]))

        return P

    def _G_h(self):
        G = np.identity(self.l)
        h = np.full(self.l, self.C)
        return G, h

    def _q(self):
        return np.full(self.l, 1.)

    def _A_b(self):
        A = np.zeros((1, self.l))
        b = np.zeros(1)
        for i in range(self.l):
            A[0, i], b[0] = self.y[i], 0
        return A, b

    def _lagrangian(self, a):
        assert len(a) == self.l
        x = self.x
        y = self.y

        s = 0
        for i in range(self.l):
            for j in range(self.l):
                s += a[i] * a[j] * y[i] * y[j] * self.kernel(x[i], x[j])
        return np.sum(a) - 1. / 2 * s

    def set_quadratic_solver(self, solver):
        self.solver = solver


class QuadraticSolverScipy:
    def __init__(self, dim):
        self.dim = dim
        self.problem = None
        self.x0 = np.zeros(self.dim)

    def set_problem(self, problem):
        self.problem = problem

    def set_liner_constraints(self, A, b, C):
        """
        Ax < b
        Ex < c
        """
        self.constraints = ({'type': 'eq', 'fun': lambda x: b - np.dot(A, x)})
        self.bounds = [(0, C) for _ in range(self.dim)]

    def solve(self):
        result = minimize(
            self.problem,
            self.x0,
            method='SLSQP',
            bounds=self.bounds,
            constraints=self.constraints
        )
        return result.x


class QuadraticSolverQuadprog:
    @staticmethod
    def quadprog_solve_qp(P, q, G=None, h=None, A=None, b=None):
        qp_G = .5 * (P + P.T)  # make sure P is symmetric
        qp_a = -q
        if A is not None:
            qp_C = -np.vstack([A, G]).T
            qp_b = -np.hstack([b, h])
            meq = A.shape[0]
        else:  # no equality constraint
            qp_C = -G.T
            qp_b = -h
            meq = 0
        return quadprog.solve_qp(qp_G, qp_a, qp_C, qp_b, meq)[0]

    @staticmethod
    def cvxopt_solve_qp(P, q, G=None, h=None, A=None, b=None):
        P = .5 * (P + P.T)  # make sure P is symmetric
        args = [cvxopt.matrix(P), cvxopt.matrix(q)]
        if G is not None:
            args.extend([cvxopt.matrix(G), cvxopt.matrix(h)])
            if A is not None:
                args.extend([cvxopt.matrix(A), cvxopt.matrix(b)])
        sol = cvxopt.solvers.qp(*args)
        if 'optimal' not in sol['status']:
            return None
        return np.array(sol['x']).reshape((P.shape[1],))


class QuadraticSolverScipyMtr:
    def __init__(self, H, c, A, b, C=np.inf, c0=0, sign=1):
        self.H = H
        self.c = c
        self.A = A
        self.b = b
        self.c0 = c0
        self.opt = {'disp':False}
        self.cons = {
            'type': 'ineq',
            'fun': lambda x: b - np.dot(A,x),
            'jac': lambda x: -A
        }
        self.bounds = [(0, C) for _ in range(c.shape[0])]
        self.sign = sign

    def _loss(self, x):
        return self.sign * (0.5 * np.dot(x.T, np.dot(self.H, x)) + np.dot(self.c, x) + self.c0)

    def _jac(self, x):
        return self.sign * (np.dot(x.T, self.H) + self.c)

    def solve(self, x0):
        res_cons = minimize(self._loss, x0, bounds=self.bounds, jac=self._jac, constraints=self.cons,
                                     method='SLSQP', options=self.opt)
        return res_cons
